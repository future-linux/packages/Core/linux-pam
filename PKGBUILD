# Maintainer: Future Linux Team <future_linux@163.com>

pkgname=linux-pam
pkgver=1.5.2
pkgrel=1
pkgdesc="PAM (Pluggable Authentication Modules) library"
arch=('x86_64')
url="https://github.com/linux-pam/linux-pam"
license=('GPL2')
groups=('base')
depends=('glibc' 'db' 'libnsl' 'cracklib' 'libtirpc')
makedepends=('bash' 'binutils' 'coreutils' 'flex' 'gcc' 'make')
backup=(etc/security/access.conf
    etc/security/faillock.conf
    etc/security/group.conf
    etc/security/limits.conf
    etc/security/namespace.conf
    etc/security/namespace.init
    etc/security/pam_env.conf
    etc/security/time.conf
    etc/environment
    etc/pam.d/other
    etc/pam.d/system-account
    etc/pam.d/system-auth
    etc/pam.d/system-password
    etc/pam.d/system-session)
options=('!emptydirs')
source=(https://github.com/linux-pam/linux-pam/releases/download/v${pkgver}/Linux-PAM-${pkgver}.tar.xz
    other
    system-account
    system-auth
    system-password
    system-session)
sha256sums=(e4ec7131a91da44512574268f493c6d8ca105c87091691b8e9b56ca685d4f94d
    9f1301b8747134c53ea191d24f60e32b1342cf3a6db27dcaa131ca4c7eb2e4f0
    adf47bccd40a58ffdb65ae0bf5113d8cc03d839faa25cc1ca8fae2d09855954e
    9bfb764a4cfcc3c74642fc10c58b36973e0115780914469cc2b9e6bc5b3b5ab9
    6c609d7317c204e4bd696cc9b968dd9a530128cf288551399cd9b0cf9a4e40dc
    520f1aeffb935e47523d957ae0b20c02e48e695573c750ca4c822d6a5c94a4da)

prepare(){
    cd Linux-PAM-${pkgver}

    sed -e 's/dummy elinks/dummy lynx/'                                    \
        -e 's/-no-numbering -no-references/-force-html -nonumbers -stdin/' \
        -i configure
}

build() {
    cd Linux-PAM-${pkgver}

    ${configure}                    \
        --sysconfdir=/etc           \
        --sbindir=/usr/sbin         \
        --enable-securedir=/usr/lib64/security

    make
}

package() {
    cd Linux-PAM-${pkgver}

    make DESTDIR=${pkgdir} install

    chmod -v 4755 ${pkgdir}/usr/sbin/unix_chkpwd

    for f in system-account system-auth system-password system-session other
    do
        install -vDm755 ${srcdir}/${f} ${pkgdir}/etc/pam.d/${f}
    done

}
